package com.example.maps_session

import android.R
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.maps_session.databinding.ActivityMainBinding
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapController
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay


class MainActivity : AppCompatActivity() {
    lateinit var bind: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bind = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bind.root)

        osmMap()
    }

    fun osmMap(){

        val map = bind.mapview

        map.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE)
        map.setBuiltInZoomControls(true)

        val mapCtrl = map.controller
        mapCtrl.setZoom(12)

        val gPt = GeoPoint(51500000, -150000)

        mapCtrl.setCenter(gPt)



    }

//    class OsmdroidDemoMap : Activity() {
//        private var mMapView: MapView? = null
//        private var mMapController: MapController? = null
//        public override fun onCreate(savedInstanceState: Bundle?) {
//            super.onCreate(savedInstanceState)
//            setContentView(R.layout.osm_main//activity)
//            mMapView = findViewById<View>(R.id.mapview) as MapView
//            mMapView!!.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE)
//            mMapView!!.setBuiltInZoomControls(true)
//            mMapController = mMapView!!.controller as MapController
//            mMapController!!.setZoom(13)
//            val gPt = GeoPoint(51500000, -150000)
//            mMapController!!.setCenter(gPt)
//        }
//    }
}